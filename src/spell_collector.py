import urllib3
from bs4 import BeautifulSoup
from ruamel.yaml import YAML

"""
List of url we don't want to visit because they are not really spells
"""
_blacklisted_url = [
    "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Vigueur.ashx",
    "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Ascension.ashx"
]

"""
This dictionnary allows to link an headline of a line in the spell text to the
key in which the matching informations will be saved.
"""
_headline_to_key = {
    "École" : "school",
    "Niveau": "level",
    "Temps d’incantation": "cast time",
    "Composantes": "component",
    "Cibles": "targets",
    "Cible": "target",
    "Durée": "duration",
    "Jet de sauvegarde": "save check",
    "Résistance à la magie": "magic resistance",
    "Description": "description",
    "Effet": "effect",
    "Portée": "range"
}

"""
This dictionnary allows to link the classes that are allowed to cast spells to
their names/nicknames.
"""
_class_to_name = {
    "Barde": ['Barde', 'barde', 'Bard', 'Bar', 'bar'],
    "Ensorceleur/Magicien": ['Ensorceleur/Magicien', 'ensorceleur/magicien', 'Ens/Mag', 'Ensorceleur/magicien', 'ens/mag', 'Ens/', 'Mag'],
    "Prêtre": ['Prê/Oracle', 'Prêtre', 'prêtre', 'Prê', 'prê', 'Pre'],
    "Rôdeur": ['Rod', 'Rôd', 'rôdeur', 'Rôdeur', 'rôd'] ,
    "Conjurateur": ['conjurateur', 'Conj'],
    "Druide": ['Drui', 'Dru', 'druide', 'Druide', 'dru'],
    "Sorcière": ['Sorcière', 'Sorc', 'Sor', 'sorcière', 'sor'],
    "Alchimiste": ['Alchimiste', 'Alc', 'Alch', 'alchimiste', 'alch', 'Alchi'],
    "Paladin": ['Paladin', 'Pal', 'paladin'],
    "Psychiste": ['Psy', 'psychiste', 'psy', 'psychique', 'Psychiste'],
    "Inquisiteur": ['Inquisiteur', 'inquisiteur', 'Inq', 'inq'],
    "Magus": ['Mag', 'Magus', 'magus', 'Mgs', 'Magu'],
    "Conjurateur": ['conjurateur', 'Conj', 'Conjurateur', 'Con'],
    "Antipaladin": ['Antipaladin', 'Anti', 'antipaladin', 'Antip', 'Antipal', 'AntiPala'],
    "Chaman": ['chaman', 'Cham', 'cha'],
    "Occultiste": ['Occ', 'Occultiste'],
    "Sanguin": ['sanguin', 'San'],
    "Oracle": ['Prê/Oracle', 'oracle'],
    "Hypnotiseur": ['hypnotiseur', "Hyp", 'Hip'],
    "Médium": ['Méd', 'Med', 'Médium'],
    "Spirite": ['Spi', 'Spirite'],
    "Adepte": ['adepte', ':', 'Inv'],
    "Enquêteur": ['Enq']
}

class SpellCollector():

    def __init__(self):
        self._http = urllib3.PoolManager()
        self. classes_found = set()

    def _fetch(self, url):
        """
        Get the html code store at the 'url' address
        """
        response = self._http.request('GET', url)
        return response.data.decode('utf-8')

    def _clean_string(self, string):
        """
        Remove all the special characters from the 'string'
        """
        return string.replace('\t', '').replace('\n', '').replace('\r', '').strip()

    def _write_spells(self, filename, data):
        """
        Write a data represented as a dictionary to the specified file. If it doesn't
        exists, the file is created.
        """
        with open(filename, 'w+') as file_:
            yaml = YAML()
            yaml.indent(sequence=6, offset=4)
            yaml.dump(data, file_)

    def collect_spells_url(self, urls, base_url):
        """
        Return a list containing all the spell url that are lised in the pages
        'urls'. Spell urls are build by adding an url find in a page to the
        'base_url'.
        For example if a page pointed by one url of 'urls' contains an url 'url1',
        the returned list will contain 'base_url'+'url1'.
        """
        spells_urls = []
        for url in urls:
            spells_page = self._fetch(url)
            spells_soup = BeautifulSoup(spells_page, 'html.parser')
            for link in spells_soup('li'):
                #Test if it is the link of a spell
                if link.i is not None:
                    spells_urls.append(base_url + link.a.get("href"))
        return spells_urls

    def extract_spells_info(self, spell_urls):
        """
        For each urls in 'spell_urls', extract all the informations gathered at
        this url. These informations are stored in a dictionnary. The list of
        dictionnaries is then returned.
        A dictionnary may contain the following keys:
            -'url': the url of the spell page
            -'name': the name of the spell
            -'book': the book where the spell come from
            -'school': the school in which the spell belongs to
            -'level': the level of the spell for each class that can learn it
            -'cast time': the cast time neccesary to launch the spell
            -'component': the components needed to lauch the spell
            -'effect': the effect of the spell when it is launched
            -'range': the range in which the spell is effective
            -'target': the object/people the spell can affect
            -'duration': the duration during which the spell is effective
            -'save check': weither the target can escape the spell thanks to a
                           save check. If it is the case, the nature of this
                           check id specified.
            -'magic resistance': weither the spell must beat the target's magic
                                 resistance in order to be effective.
            -'description': the complete description of the spell
        """
        info_list = []
        for url in spell_urls:
            # Ensure we don't visit blacklisted url
            if url not in _blacklisted_url:
                spell_page = self._fetch(url)
                spell_page = self._preprocess_raw_html(spell_page)
                spell_soup = BeautifulSoup(spell_page, "html5lib").find(id="content")
                # Handles particular case, the spell https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Forme%20de%20vase%20I.ashx
                if spell_soup.find(class_="chapeau CS") != None:
                    spell_soup.find(class_="chapeau CS").replace_with("")
                spell_info = {"url": url}
                self._extract_page_info(spell_info, spell_soup)
                info_list.append(spell_info)
        return info_list

    def _preprocess_raw_html(self, html_page):
        """
        Pre-process the raw html before it is parse by the BeautifulSoup4. This
        allows to handle some special cases.
        """
        # Handles particular case, the spell https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Analyse%20daura.ashx
        html_page = html_page.replace("<br />\n<br />", "<br /><br />")
        # Handles particular case, the spell https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Armure%20de%20sang.ashx
        html_page = html_page.replace("<br/>\n<br/>", "<br/><br/>")
        return html_page

    def _extract_page_info(self, info_dict, spell_soup):
        """
        Extract all the spell informations contained in the 'spell_soup' and
        store them in the 'info_dict'.
        """
        info_dict["name"] = self._clean_string(spell_soup.find(class_="pagetitle").string)
        spell_section = spell_soup.find(id="PageContentDiv")
        info_dict["book"] = self._get_spell_book(spell_section)
        spell_text = self._get_spell_text(spell_section)
        # Handle particular case when "Description" contains some '\n'. It works
        # because we know that each spell has a "description" paragraph.
        spell_text, description = spell_text.split("Description")
        info_dict[_headline_to_key["Description"]] = description
        for line in spell_text.split('\n'):
            line = line.strip()
            for headline in _headline_to_key:
                if line.startswith(headline):
                    self._extract_line_info(info_dict, headline, line)
                    break
        self._level_to_list(info_dict)
        self._unify_school_name(info_dict)

    def _unify_school_name(self, info_dict):
        """
        Unify the name of the magic school of the spells in order to have different
        names for the same school.
        """
        info_dict['school'] = info_dict['school'].strip().capitalize()
        #Handle special case
        if info_dict['school'] == 'Inv':
            info_dict['school'] = 'Invocation'

    def _level_to_list(self, info_dict):
        """
        Post-treat the "level" key of the 'info_dict'. Transform the string that
        represents the levels into a list of strings. Each of these new strings
        contains a name of class and the level of the spell for this class.
        For exemple, "Bard 3, Ens/Mag 3" becomes ["Bard 3", "Ens/Mag 3"].
        """
        levels = info_dict['level'].split(', ')
        info_dict['level'] = []
        for class_level in levels:
            class_ = self._clean_string(class_level).split(' ')[0]
            level = self._clean_string(class_level).split(' ')[1].replace(',', '')
            for key in _class_to_name:
                if class_ in _class_to_name[key]:
                    class_ = key
                    break
            self.classes_found.add(class_)
            info_dict['level'].append("{} {}".format(class_, level))

    def _get_spell_text(self, spell_section):
        """
        Prepare the 'spell_section' and extract the spell text from it.
        """
        # Add a "Description" tag to simplly extract it later
        self._tag_description_start(spell_section)
        # Clean the text
        text = spell_section.get_text().strip('\n').strip('\t').strip('\n')
        # Force lines with 2 headlines to be split
        text = text.replace(';', '\n')
        # Handles particular case, the spell "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.D%c3%a9sir%20anormal.ashx"
        text = text.replace('Niveau\n', 'Niveau ')
        # Handles particular case, the spell "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Narcissisme%20suffisant.ashx"
        text = text.replace('barde\n', 'barde ')
        # Handles particular case, the spell "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Anticipation%20des%20pens%c3%a9es.ashx"
        text = text.replace('Mag\n', 'Mag ')
        text = text.replace('Sor\n', 'Sor ')
        # Handles particular case, the spell https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Infliger%20des%20souffrances.ashx
        text = text.replace('Méd\n', 'Méd ')
        # Handles particular case, the spell https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Modification%20daura.ashx
        text = text.replace('Psy\n', 'Psy ')
        # Handles particular case, the spell https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Offrande%20exig%c3%a9e.ashx
        text = text.replace('AntiPala\n', 'AntiPala ')
        # Handles particular case, the spell https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Pi%c3%a8ge%20ectoplasmique.ashx
        text = text.replace('Magus\n', 'Magus ')
        # Handles particular case, the spell https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.R%c3%a9trocognition.ashx
        text = text.replace('Occ\n', 'Occ ')
        # Handles particular case, the spell https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.R%c3%a9trocognition.ashx
        text = text.replace('École :', 'École')
        text = text.replace('Niveau :', 'Niveau')
        return text

    def _tag_description_start(self, spell_section):
        """
        Add a line return and the tag "Description" at the beginning at the
        description section.
        """
        for br in spell_section.find_all("br"):
            if br.next_sibling.name == "br":
                if not br.next_sibling.next_sibling.name == "b":
                    # Check if the "br" element is not before the spells info
                    br.insert_after("\nDescription ")
                    return
                # Insert "\n" after the "br" element to avoid sticky text
                br.insert_after("\n")
        return

    def _extract_line_info(self, info_dict, headline, line):
        """
        Extract the informations of the 'line' located after the 'headline'.
        These informations are stored in the 'info_dict' which the key is given
        by the '_headline_to_key' dict.
        """
        info = line.replace(headline, '')
        info_dict[_headline_to_key[headline]] = self._clean_string(info)

    def _get_spell_book(self, spell_section):
        """
        Extract the name of the book which the spell represented by the
        'spell_section' comes from. This name is one of the following:
            -"Player Handbook/Manuel des joueurs"
            -"Ultimate Magic/Art de la Magie"
            -"Advanced Player's Guide/Manuel du joueur - règles avancées"
            -"Ultimate Combat/Art de la Guerre"
            -"Advanced Class Guide/Manuel du joueur - classes avancées"
            -"Mythic Campaign/Campagnes mythiques"
            -"Occult adventures/Aventures occultes"

            We cannot spot when a spell is in the "Mythic Campaign/Campagnes mythiques"
            book. We class those spells in the base book where they originally come
            from. If there is no original book, they are classed in the
            "Player Handbook/Manuel des joueurs" book.
        """
        book = "Player Handbook/Manuel des joueurs"
        images = spell_section.find_all("img", class_='opachover')
        if len(images) == 1:
            if not (spell_section.h2 is not None and "class" in spell_section.h2.attrs):
                # There is only 1 paragraph and only 1 image
                book = images[0]["title"].replace("Source : ", '')
            # Else, there is 2 paragraph and 1 image so we class as a PHB spell
        elif len(images) == 2:
            # There is 2 paragraphs and 2 images so we only consider the 1st one.
            book = images[0]["title"].replace("Source : ", '')
        return book

if __name__ == '__main__':
    urls = ['https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Liste%20des%20sorts.ashx',
            'https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Liste%20des%20sorts%20(suite).ashx',
            'https://www.pathfinder-fr.org//Wiki/Pathfinder-RPG.Liste%20des%20sorts%20(fin).ashx'
            ]
    base_url = 'https://www.pathfinder-fr.org/Wiki/'
    spell_collector = SpellCollector()
    spells_urls = spell_collector.collect_spells_url(urls, base_url)
    spells_info = spell_collector.extract_spells_info(spells_urls)
    #print(spell_collector.classes_found)
    spell_collector._write_spells("spells.yaml", spells_info)
