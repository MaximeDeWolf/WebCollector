import jinja2
import os
#from jinja2 import Template
from ruamel.yaml import YAML
from ruamel import yaml
from spell_collector import _class_to_name

"""
#http://eosrei.net/articles/2015/11/latex-templates-python-and-jinja2-generate-pdfs
latex_jinja_env = jinja2.Environment(
	block_start_string = '\BLOCK{',
	block_end_string = '}',
	variable_start_string = '\VAR{',
	variable_end_string = '}',
	comment_start_string = '\#{',
	comment_end_string = '}',
	line_statement_prefix = '%%',
	line_comment_prefix = '%#',
	loader = jinja2.FileSystemLoader(os.path.abspath('.'))
)
"""

"""
This list contains some tuple of size 2. Each tuple contains a character to
replace by the second character.
"""
_escape_list = [
    ("°", "$^{\circ}$"),
    ("%", "\%"),
    ("³", "$^3$"),
    ("²", "$^2$"),
    ("&", "\&"),
    ("½", "$\\frac{1}{2}$"),
    ("×", "$\\times$"),
    ("#", "")
]

"""
This list contains some tuples of size 2. Each tuple contains a school of magic
(with a capital letter) and the corresponding Latex command to create the appropriate
card.
"""
_school_latex_command = {
    "Abjuration": "\cardtypeAbjuration",
    "Invocation": "\cardtypeConjuration",
    "Divination": "\cardtypeDivination",
    "Enchantement": "\cardtypeEnchantment",
    "Évocation": "\cardtypeEvocation",
    "Illusion": "\cardtypeIllusion",
    "Nécromancie": "\cardtypeNecromancy",
    "Transmutation": "\cardtypeTransmutation",
    "Universel": "\cardtypeUniversal"
}


class LatexInjector():

    def __init__(self, data_file, template_file):
        env = jinja2.Environment(
                loader=jinja2.FileSystemLoader(os.path.abspath('.')),
            	trim_blocks = True,
                lstrip_blocks = True,
                variable_start_string = '\VAR{',
            	variable_end_string = '}'
            )
        self.template = env.get_template(template_file)
        self.spells_data = self._open_yaml(data_file)

    def _open_yaml(self, yaml_file):
        """
        Open a file in YAML format and represents it as a dictionary
        """
        with open(yaml_file, 'r') as file_:
            yaml_content = yaml.safe_load(file_)
        return yaml_content

    def filter_data(self, filter_):
        """
        Select the 'spells_data' which return 'True' when the 'filter_' function
        is applied on it.
        """
        self.spells_data = filter(filter_, self.spells_data)

    def compute_context(self):
        """
        Compute the dictionnary of informations that will be used by Jinja
        to render the template. It contains a key: "root". The value of this key
        is a list of dictionnaries that represents a class. It has a "name" key
        wich value is the name of the class and a key "batches" that contains
        a list of list of spells. These sublists represent a line of spells in the
        template.

        Ex:

        root:
            - name: "Paladin"
              batches:
                -
                    - spell1
                    - spell2
                    - spell3
                    - spell14
                - ...
        """
        classes_dict = self._compute_classes_dict()
        self._sort_classes_dict(classes_dict)
        self._make_batch(classes_dict, 4)
        context = {"root": []}
        for class_ in classes_dict:
            context["root"].append({"name": class_, "batches": classes_dict[class_]})
        return context

    def _sort_classes_dict(self, classes_dict):
        """
        Sort the spells that belongs to the same class by level.
        """
        for class_ in classes_dict:
            classes_dict[class_].sort(key=lambda spell: spell["level"])

    def _make_batch(self, classes_dict, batch_size):
        """
        For each key of the 'classes_dict', transform its list of spells into
        a list of list of spells. These sublists contains 'batch_size' elements
        at most.

        Ex. with 'batch_size' = 4:

        "Paladin":
            -
                - spell1
                - spell2
                - spell3
                - spell14
            - ...
        """
        for class_ in classes_dict:
            batches = []
            for start_batch_index in range(0, len(classes_dict[class_]), batch_size):
                batch = []
                for current_index in range(start_batch_index, start_batch_index + batch_size):
                    if current_index >= len(classes_dict[class_]):
                        break
                    else:
                        batch.append(classes_dict[class_][current_index])
                batches.append(batch)
            classes_dict[class_] = batches

    def _compute_classes_dict(self):
        """
        Compute a dictionnary which each key is a class and their value is a list
        of spells that this class can cast. Each spell can contains the following
        keys (/!\ these keys are not exactly the same that are produced in
        'latex_injector.py'):
            -'url': the url of the spell page
            -'name': the name of the spell
            -'school': the school in which the spell belongs to
            -'level': a number, the level of the spell for this class
            -'cast time': the cast time neccesary to launch the spell
            -'component': the components needed to lauch the spell
            -'effect': the effect of the spell when it is launched
            -'range': the range in which the spell is effective
            -'target': the object/people the spell can affect
            -'duration': the duration during which the spell is effective
            -'save check': weither the target can escape the spell thanks to a
                           save check. If it is the case, the nature of this
                           check id specified.
            -'magic resistance': weither the spell must beat the target's magic
                                 resistance in order to be effective.
            -'description': the complete description of the spell

        Ex:

        Paladin:
            - url: ...
              level: ...
              .
              .
              .
            - url: ...
              .
              .
              .
        """
        classes_dict = self._create_classes_dict()
        for spell in self.spells_data:
            self._pretreat_spell(spell)
            for level_line in spell['level']:
                class_, level = level_line.split(' ')
                spell_class_info = spell.copy()
                spell_class_info['level'] = level
                classes_dict[class_].append(spell_class_info)
        self._delete_empty_classes(classes_dict)
        return classes_dict

    def _pretreat_spell(self, spell):
        """
        Pretreat the 'spell' dictionnary in order to make its values rendered
        properly by Latex
        """
        # We don't need the 'book' key anymore
        del spell['book']
        self._school_to_latex(spell)
        self._escape_special_characters(spell)

    def _school_to_latex(self, spell):
        """
        Transform the school of the 'spell' into the appropriate Latex command to
        render it properly.
        """
        school = spell['school'].replace('(', ' (').split(' ')[0]
        spell['school'] = _school_latex_command[school]

    def _escape_special_characters(self, spell):
        """
        Escape the special characters of Latex by their Latex traduction.
        """
        for key in spell:
            # No need to escape characters for the 'level' key + its value is a list
            if key != 'level':
                for to_replace, replace_by in _escape_list:
                    spell[key] = spell[key].replace(to_replace, replace_by)

    def _delete_empty_classes(self, classes_dict):
        """
        Delete the classes in 'classes_dict' which associated spells list is
        empty.
        """
        to_delete = []
        for class_ in classes_dict:
            if len(classes_dict[class_]) == 0:
                to_delete.append(class_)
        # we cannot change the number of keys in dictionnary while itarating on it.
        for class_ in to_delete:
            del classes_dict[class_]

    def _create_classes_dict(self):
        """
        Create a dictionnary which each key is a class and the value is a empty
        list.
        """
        classes_dict = {}
        for class_ in _class_to_name:
            classes_dict[class_] = []
        return classes_dict

    def inject_data(self, context, output_file):
        """
        Render the 'self.template' with the data contained in the 'context'
        dictionnary. Then, the results is written in a the 'output_file'. If
        this file doesn't exist, it is created.
        """
        rendered_data = self.template.render(context)
        with open(output_file, 'w+') as file_:
            file_.write(rendered_data)

def select_player_handbook(spell):
    return spell["book"] == "Player Handbook/Manuel des joueurs"

if __name__ == '__main__':
    #TODO: in latex_injector: replace the school of a spell the Latex command(_school_command)
    latex_injector = LatexInjector("spells.yaml", "spells_cards_template.tex")
    latex_injector.filter_data(select_player_handbook)
    context_data = latex_injector.compute_context()
    latex_injector.inject_data(context_data, "output/spells_cards.tex")
    """
    spell_cards = latex_injector.inject(context_data, "spells_cards.tex")
    print(spell_cards)
    """
