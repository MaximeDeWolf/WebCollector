# SpellCollector

**SpellCollector** is a little Python 3 script that browses the pages that list all the spells of the [Pathfinder website](https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Liste%20des%20sorts.ashx). Then it parses the page of each of these spells in order to collect some informations such as the target, the range or the components of the spell. These informations are stored in a **YAML** file.

Then these informations are used to complete a **Latex** template to turn them into game cards. This way, Pathfinder players can easily print the cards they are interested in (or keep them in a digital copy). Also, they have an overview of all the spells their character can cast.

The latest produced PDF is available on the official French Pathfinder wiki [here](https://www.pathfinder-fr.org/wiki/GetFile.aspx?File=/ADJ/Pathfinder-RPG/CartesSortsMDW.pdf).

All the informations gathered are in French and stored in the file *spells.yaml*. These informations are the property of [Black Book Editions](http://www.black-book-editions.fr/), [Paizo Publishing](https://paizo.com/) and [Pathfinder-fr.org](https://www.pathfinder-fr.org/) and are thus under the [Open Game License](https://www.pathfinder-fr.org/Wiki/OGL.ashx).

![alt text][black_book] ![alt text][paizo] ![alt text][pathfinder]

[black_book]: https://paradoxetemporel.fr/wp-content/uploads/2018/05/black-book-editions.png "Black Book Editions"
[paizo]: http://img4.wikia.nocookie.net/__cb20080929145519/pathfinder/images/c/c8/Paizo_logo.jpg "Paizo Publishing"
[pathfinder]: https://pbs.twimg.com/profile_images/1121716661/PFR_400x400.png "Pathfinder-fr.org"

## Goal

The goal of **SpellCollector** is to create game cards that represents the spells of Pathfinder. This is helpful for the players. Indeed it allows them to keep on eye on their spells and the peculiarities of each one.

## Dependencies
The project **SpellCollector** needs the packages **Pyhton3**, **pdftk** and **texlive-full** to be installed in order to work properly.

**SpellCollector** depends on a few **Python3** libraries:
* urllib3
* BeautifulSoup4
* ruamel
* Jinja2

You can install them by running the following command: *pip3 install -r requirements.txt*.

The "database" *spells.yaml* and the **Latex** file *output/spells_cards.tex* are stored with [Git LFS](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/), so before cloning this project you have to install Git LFS and initialize it with the command `git lfs install`.

## Usage

This is the list of commandes that are supported by our program, all these
commandes must be executed from the from the root of the project:
- To update/create the file *spells.yaml*, run the command `make collect`
- To create/update the file **output/spells_cards.tex**, run the command `make inject`
- To create the PDF **output/spells_cards.pdf**, **output/intro.pdf** and **output/OGL.pdf**, run the command `make compile`. This command will first lauch the `make inject` command before being executed
- To create the PDF **output/intro_spells_cards.pdf**, run the command `make merge`
- Finally, the command `make all` will lauch all the previously described commands

## Credit

 - The Latex cards package was made by [Krozark](https://github.com/Krozark/RPG-LaTeX-Template) and then modified by [bastienvanderplaetse](https://github.com/bastienvanderplaetse) and me.

 - The symbols associated with the school of the spells come from [Games-icons.net](https://game-icons.net/)

 - The cover image of the *output/intro.pdf* was created by [cromaticresponses
 ](https://www.deviantart.com/cromaticresponses/art/Pathfinder-fan-art-the-Witch-282630925)

## Known bugs
 - Spells that appear in the "Campagne mythique" are classed following the original
 book they come from. If they don't have original book, they are classed as "Player Handbook".
 - The spells from all the books are present in the generated PDF.
