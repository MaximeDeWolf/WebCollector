collect:
	echo Downloading the spells, this action can take +/- 10 minutes
	python3 src/spell_collector.py

inject:
	echo generating the spell cards PDF, this may take some time
	python3 src/latex_injector.py

compile:
	echo re-compiling introduction, spells_cards and OGL PDF. This may take some time...
	cd output/; lualatex intro.tex; lualatex OGL.tex; pdflatex spells_cards.tex

merge: compile
	echo merging introduction, the spell cards and the OGL
	pdftk output/intro.pdf output/spells_cards.pdf output/OGL.pdf cat output output/Spells_Cards.pdf

all: collect inject merge
